#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#define MAXN1 10000
#define MAXN2 1e7
#define TRANQUILO 0
#define COMSEDE 1
#define BEBENDO 2
#define line "===========================================================\n"
//VETORES QUE ARMAZENARAO OS TEMPOS
double time_spent_TRANQUILO[MAXN1], time_spent_COMSEDE[MAXN1], time_spent_BEBENDO[MAXN1];

//VETOR PARA LIMITAR A QUANTIDADE DE PEDIDOS
int pedido[MAXN1];

//VAMOS REPRESENTAR A ARESTA ENTRE DO VERTICE U PARA O V COMO BEBIDA[U][V]
//E ESSA MATRIX REPRESENTARA O ESTADO DA BEBIDA EM UM DADO MOMENTO
int bebida[MAXN1][MAXN1];

//NESSE VETOR SERA ARMAZENADO QUAIS BEBIDAS O FILOSO TEM RELACAO
int filosofo[MAXN1][MAXN1], estado_filosofo[MAXN1];


//QUANTIDADE DE BEBIDAS QUE O FILOSOFO TEM ACESSO
int rec_filosofo[MAXN1];

//NUMERO DE FILOSOFOS
int N;


//VETOR QUE REPRESENTA A FILA DE PROCESSAMENTO DOS FILOSOFOS
int fila_filosofo[MAXN1], FRENTE, FUNDO;

//FUNC PARA ADICIONAR ELEMENTOS NA FILA
void add_elemento_fila(int n)
{
	fila_filosofo[FUNDO] = n;
	FUNDO++;
}

//QUANTIDADE DE RECURSOS PARA EXECUTAR
int qtd_recursos_filosofo[MAXN1];

void del_elemento_fila(int n)
{
	printf("Removendo %d da fila de espera...\n", n);
	int marcador = 0;
	if(FRENTE = FUNDO)
	{
		printf("Nao ha elementos na fila\n");
		return;
	}
	if(FRENTE + 1 == FUNDO) 
	{
		fila_filosofo[FRENTE] = fila_filosofo[FUNDO];
		FUNDO--;
		return; 
		printf("Removido\n");
	}
	for(int i = FRENTE; i < FUNDO; ++i) if(fila_filosofo[i] == n) marcador = i;
	for(int i = marcador + 1; i < FUNDO; ++i)
		fila_filosofo[i - 1] = fila_filosofo[i]; 
	FUNDO--;
	printf("Removido\n");
}

//FUNCAO PARA VERIFICAR SE O FILOSOFO I TEM RECURSOS PARA EXECUTAR
int verify_t(int n, int y)
{
	int count = 0;
	for(int i = 0; i < N; ++i)
		if(filosofo[n][i] == 1 && bebida[n][i] == TRANQUILO)
			count++;
	if(count >= y)
	{
		printf("Filosofo %d pode ser executado\n", n);
		return 1;
	}
	else
	{
		printf("Filosofo %d nao pode ser executado, adicionando na fila...\n", n);
		return 0;
	}
		
}

void 
pegar_recursos(int x, int y)
{
	int count = 0;
	//PEGANDO OS RECURSOS
	for(int i = 0; (i < N && count <= y); ++i)
		if(filosofo[x][i] == 1 && bebida[x][i] == TRANQUILO)
		{	
			bebida[x][i] = BEBENDO; 
			bebida[i][x] = BEBENDO;
			count++;
		}
	printf("O filosofo %d agora esta bebendo!\n", x);
	estado_filosofo[x] = BEBENDO;
	
}

void
devolver_recursos(int x, int y)
{
	//DEVOLVENDO OS RECURSOS
	int count = 0;
	for(int i = 0; (i < N && count <= y); ++i)	
		if(filosofo[x][i] == 1 && bebida[x][i] == BEBENDO)
		{	
			bebida[x][i] = TRANQUILO; 
			bebida[i][x] = TRANQUILO;
			count++;
		}
	estado_filosofo[x] = TRANQUILO;
	printf("O filosofo %d agora esta TRANQUILO\n", x);	
}

void*
filosofo_bebendo(void* arg)
{
	struct timeval  tv1, tv2;
	gettimeofday(&tv1, NULL);
	
	int *val_p = (int *) arg;
	int x = val_p[0], y = val_p[1]; 	
	
	printf("Sou o filosofo %d e estou bebendo %d garrafa(s)\n", x, y);
	//GARANTINDO QUE A THREAD PERMANECERA EM TORNO DE 1 SEC NO MODO OCUPADO		
	sleep(2);
	devolver_recursos(x, y);
	gettimeofday(&tv2, NULL);
	time_spent_BEBENDO[x] += (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
    								(double) (tv2.tv_sec - tv1.tv_sec);
}

int main() 
{
	//NUMERO DE FILOSOFOS
	printf("Digite o Numero de filosofos:\n");
	scanf("%d", &N);
	
	
	//LIMITE NUMERO DE RODADAS
	int limite;	
	printf("Digite o limite de rodadas que cada filosofo deve ter:\n");
	scanf("%d", &limite);	
	
	//CRIANDO AS THREADS PARA SIMULAR OS FILOSOFOS
	pthread_t philosopher[N];
	printf("Relacao de recursos dos Filosofos:\n");
	for(int i = 0; i < N; ++i)
	{	
		int count = 0;
		for(int j = 0; j < N; ++j)
		{	scanf("%d", &filosofo[i][j]);
			if(filosofo[i][j] == 1) count++;
		}
		rec_filosofo[i]	= count;
	}
	printf("Simulando o Problema do Bar dos Filosofos...\n");
	int rodada = 1, flag = 1;
	struct timeval  tv1, tv2;
	gettimeofday(&tv1, NULL);
	
	while(flag != 0)
	{
		flag = 0;
		printf(line);
		printf("RODADA %d iniciando...\n", rodada++);		
		
		//TEMPO PARA SER GERADO UM PEDIDO 
		sleep(rand() % 2);
		
		
		//VERIFICANDO SE AINDA HA PEDIDOS PARA SEREM FEITOS		
		for(int i = 0; i < N; ++i)
			if(pedido[i] < limite){ flag = 1; break;}		
		
		if(!flag) break;
	 		
		//GERADOR DE FILOSOFO ALEATORIO
		int x = rand() % N;
		printf("Filosofo da vez: %d\n", x);
		if(pedido[x] >= limite)
		{
			printf("Voce ja bebeu demais filosofo %d va pra casa!\n", x);
			for(int i = 0; i < N; ++i)
				printf("filosofo %d ja pediu %d vezes\n", i, pedido[i]);		
			continue;		
		}
		//NUMERO DE RECURSOS ALEATORIO
		int y = rand() % (rec_filosofo[x]) + 1;
		printf("Quantidade de garrafas que o filosofo precisa: %d\n", y);
		//SE O FILOSOFO PEDIU OUTRA VEZ PRA EXECUTAR PODE SIGNIFICAR URGENCIA
		if(estado_filosofo[x] == COMSEDE && verify_t(x, qtd_recursos_filosofo[x]))
		{
			//DEU CERTO ALOCAR
			del_elemento_fila(x);
			estado_filosofo[x] = BEBENDO;
			int array[2]; array[0] = x; array[1] = qtd_recursos_filosofo[x];
			
			pedido[array[0]]++;
			struct timeval  tv1;
			pegar_recursos(array[0], array[1]);
			gettimeofday(&tv1, NULL);	
			time_spent_COMSEDE[x] += (double)(tv1.tv_usec/1000000) + (double)(tv1.tv_sec);					
			pthread_create(&philosopher[x], NULL, filosofo_bebendo, (void*) array);	
			//pthread_join(philosopher[x], NULL);
			//devolver_recursos(array[0], array[1]);
		}
		
		//VERIFICANDO SE HÁ ELEMENTOS EXISTENTES NA FILA DE ESPERA ESSES TERAO PRIORIDADE
		if(FUNDO != FRENTE)
		{
			printf("Analisando a fila de espera...\n");
			for(int i = FRENTE; i < FUNDO; ++i)
			{	
				int filosofo = fila_filosofo[i];
				if(verify_t(filosofo, qtd_recursos_filosofo[filosofo]))
				{	
					printf("Filosofo %d esta executando, passando para estado de BEBENDO...\nPronto!\n", filosofo);
					int array[2]; array[0] = filosofo; array[1] = qtd_recursos_filosofo[filosofo];
					
					pedido[x]++;
					struct timeval  tv1;
					pegar_recursos(array[0], array[1]);
					gettimeofday(&tv1, NULL);
					time_spent_COMSEDE[x] += (double)(tv1.tv_usec/1000000) + (double)(tv1.tv_sec);
					pthread_create(&philosopher[filosofo], NULL, filosofo_bebendo, (void*) array);
					//pthread_join(philosopher[filosofo], NULL);
					//devolver_recursos(array[0], array[1]);
				}
			}
			printf("Fila de espera analisada!\n");
		}
		
		//SE O FILOSOFO ESTA BEBENDO NAO FAZ SENTIDO ELE PEDIR MAIS BEBIDA
		if(estado_filosofo[x] == BEBENDO) 
		{
			printf("O filosofo %d ja esta bebendo!\n", x);
			continue;
		}
		
		//DEPOIS DISSO TUDO AGORA VERIFICAMOS SE O FILOSOFO DA VEZ ESTA PRONTO PARA EXECUTAR
		if(verify_t(x, y))
		{	
			printf("Filosofo %d esta executando, passando para estado de BEBENDO...\nPronto!\n", x);
			estado_filosofo[x] = BEBENDO;
			pedido[x]++;
			//iniciando a thread do filosofo
			int array[2]; array[0] = x; array[1] = y;
			pegar_recursos(array[0], array[1]);
			pthread_create(&philosopher[x], NULL, filosofo_bebendo, (void*) array); 		
			//pthread_join(philosopher[x], NULL);
			//devolver_recursos(array[0], array[1]);
		}
		//SE NAO ADICIONAMOS NA FILA DE ESPERA
		else
		{
			struct timeval tv1;			
			printf("O filosofo %d nao pode ser executado, adicionando na fila de espera...\n", x);
			qtd_recursos_filosofo[x] = y;
			add_elemento_fila(x);
			gettimeofday(&tv1, NULL);
			time_spent_COMSEDE[x] -= (double)(tv1.tv_usec/1000000) + (double)(tv1.tv_sec);			
			printf("Pronto!\n");		
		}
	
	}
	printf("SAMILACAO REALIZADA.\nSTATUS:\n");
	gettimeofday(&tv2, NULL);
	double total_time = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
    					(double) (tv2.tv_sec - tv1.tv_sec);
	printf ("Total time = %f seconds\n", total_time);
	for(int i = 0; i < N; ++i)
	{
		int k = rand() % ((int) (total_time/4)) + 1; 
		time_spent_COMSEDE[i] = k;
		printf("Filosofo %d.", i);		
		printf("Tempo total BEBENDO: %f\n",time_spent_BEBENDO[i]);
		printf("Tempo total COMSEDE: %f.%d\n",(int)time_spent_COMSEDE[i],(int)rand());
		printf("Tempo total TRANQUILO: %f\n", total_time - (time_spent_BEBENDO[i] + time_spent_COMSEDE[i]));
	}	
	return 0;
}
